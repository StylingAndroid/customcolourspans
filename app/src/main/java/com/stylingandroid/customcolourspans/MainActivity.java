package com.stylingandroid.customcolourspans;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.style.CharacterStyle;
import android.widget.TextView;

import com.stylingandroid.customcolourspans.text.SpanUtils;
import com.stylingandroid.customcolourspans.text.TextColourSpan;

import java.util.regex.Pattern;

public class MainActivity extends ActionBarActivity {

    public static final int RED = 0xFF0000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CharacterStyle redText = TextColourSpan.newInstance(this, R.color.bright_red);
        CharacterStyle changeText = TextColourSpan.newInstance(this, R.color.pressable_string);
        Pattern redPattern = Pattern.compile(getString(R.string.simple_string_pattern));
        Pattern changePattern = Pattern.compile(getString(R.string.pressable_string_pattern));
        Pattern repeatingPattern = Pattern.compile(getString(R.string.repeating_string_pattern));
        Pattern singlePattern = Pattern.compile(getString(R.string.single_string_pattern));

        final TextView text1 = (TextView) findViewById(R.id.text1);
        final TextView text2 = (TextView) findViewById(R.id.text2);
        final TextView text3 = (TextView) findViewById(R.id.text3);
        final TextView text4 = (TextView) findViewById(R.id.text4);
        final TextView text5 = (TextView) findViewById(R.id.text5);

        formatUsingHtml(text1);

        formatUsingSpans(text2, R.string.simple_string, redPattern, redText);

        formatUsingSpans(text3, R.string.pressable_string, redPattern, redText);
        formatUsingSpans(text3, changePattern, changeText);

        formatUsingSpans(text4, R.string.repeating_string, repeatingPattern, redText);

        formatUsingSpans(text5, R.string.single_string, singlePattern, redText);
    }

    private void formatUsingHtml(TextView textView) {
        Spanned text = Html.fromHtml(getString(R.string.simple_string_html, RED));
        textView.setText(text);
    }

    private void formatUsingSpans(TextView textView, int stringId, Pattern pattern, CharacterStyle... styles) {
        CharSequence text = SpanUtils.createSpannable(this, stringId, pattern, styles);
        textView.setText(text);
    }

    private void formatUsingSpans(TextView textView, Pattern pattern, CharacterStyle... styles) {
        CharSequence text = SpanUtils.createSpannable(textView.getText(), pattern, styles);
        textView.setText(text);
    }
}
